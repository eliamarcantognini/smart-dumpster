
# Dumpster Edge

Connection handler for the Dumpster system.

## Instructions

Set variables for the right connection.

### File config.h

Set 'SSID' variable with the name of the network which the esp has to connect.
Set 'PWD' variable with the password of the wifi network.

### File main.cpp

Set 'server' variable with the IP address of the local machine in the connected network.
